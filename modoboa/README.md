**Note:** This container is automatically built by [*docker-image-rebuilder*](https://gitlab.com/ntninja/docker-image-rebuilder) every night and will receive updates whenever the upstream Modoboa or Alpine packages change. [(Dockerfile)](https://gitlab.com/ntninja/docker-recipies/blob/master/modoboa/Dockerfile)

# Modoboa Container Set

## Setup

Download and extract the quickstart example directory:

```sh
wget "https://gitlab.com/ntninja/docker-recipies/raw/master/modoboa/quickstart.zip"
unzip quickstart.zip
mv quickstart modoboa
cd modoboa
```

…most importantly it will contain a [`docker-compose.yml` file](https://gitlab.com/ntninja/docker-recipies/blob/master/modoboa/quickstart/docker-compose.yml). Feel free to adapt the files to your needs!

### Modoboa

Run the following command to deploy a new modoboa instance (only one instance per container is supported):

```shell
docker-compose run modoboa deploy --collectstatic \
         --domain <hostname of your server> --dburl <mysql|postgres>://[user:pass@][host:port]/dbname
```

Make sure that you have a PostgreSQL or MySQL database ready and on the same network as your Modoboa services before doing this. Only PostgreSQL has been tested, MySQL should work as well through; SQLite may work but likely has issues.

### Dovecot

Pre-generate the Dovecot TLS Diffie-Hellman parameters using the following command:

```shell
openssl dhparam -out ./dovecot/config/dh.pem 4096
```

### Start and Enjoy

After starting the the composition using `docker-compose up` the Modoboa container will listen for HTTP requests (not uWSGI) on port 8080.

Depending on the installed plugins (see below) you may have to tweak some settings under *Modoboa* → *Parameters*:

  * `modoboa-sievefilters`
       - Tab *SIEVE-Filters*, section *ManageSieve-Settings*:
           * Server-Address: **dovecot**
           * Server-Port: **4190**
           * Use StartTLS: **No** (Data will only be sent through Docker's local loopback interfaces)
       - Tab *SIEVE-Filters*, section *IMAP-Settings*:
           * Server-Address: **dovecot**
           * Secure Connection: **No** (Data will only be sent through Docker's local loopback interfaces)
           * Server-Port: **143**
  * `modoboa-stats`
       - All default settings (`/var/log/mail.log` & `/tmp/modoboa`) are implemented by the container – don't change these paths!
  * `modoboa-webmail`
       - Tab *Webmail*, section *IMAP-Settings*:
           * Server-Address: **dovecot**
           * Secure Connection: **No** (Data will only be sent through Docker's local loopback interfaces)
           * Server-Port: **143**
       - Tab *Webmail*, section *SMTP-Settings*:
           * Server-Address: **postfix**
           * Secure Connection: **None** (Data will only be sent through Docker's local loopback interfaces)
           * Server-Port: **587**

 

## Management

All important management scripts should be available using the `docker-compose run modoboa …`:

 * Running the `modoboa-admin.py` script:
    * `docker-compose run modoboa admin <options>`
    * Note that you will likely not need this command, as upgrades and deployments can be done using separate commands instead (see the relevant sections)
 * Running `python3 manage.py` on the deployed instance:
    * `docker-compose run modoboa manage <options>`
 * Installing Modoboa [Python modules/extensions](https://github.com/modoboa/modoboa#main-features):
    * `docker-compose run modoboa install <name>`

## Upgrading

Edit the version number in the `docker-compose.yml` file, then stop your modoboa setup, using `docker-compose down` or similar, and execute the following commands in the modoboa directory:

```sh
# Update image
docker-compose pull

# Update installed Python plugins
docker-compose run modoboa update

# Migrate data and refresh static file directory
docker-compose run modoboa manage migrate
echo yes | docker-compose run modoboa manage collectstatic
```

### Upgrade notes: Modoboa 1.13

The Postfix container now uses the `ntninja/modoboa-postfix:latest` image (rather then the previous `ntninja/alpine-postfix` image). Please update your compose file to reflect this or the stats plugin will not work.

The “TLS setup” of the Postfix QuickStart file changed to enable opportunistic DNSSEC/DANE validation support when the receiving host supports this. The new configuration section now reads:

```
# TLS server setup
smtpd_use_tls = yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache

# TLS client setup using opportunistic DNSSEC/DANE validation when supported by the receiving server
smtp_dns_support_level = dnssec
smtp_tls_security_level = dane
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
```

Also, installing a validating DNS resolver, such as [Unbound](https://nlnetlabs.nl/projects/unbound/), on the host is recommended to ensure that DNSSEC validation is effective. As a side-effect this may also speed up Postfix' message DNS checks in general if you previously did not have any caching DNS resolver installed locally.

See also the [Modoboa Upgrade Notes](https://modoboa.readthedocs.io/en/latest/upgrade.html#id2) for other required steps.

## Getting Help / Reporting Issues

While I will not guarantee any kind of timely response, please do open issues with questions and suggestions [on the GitLab repo](https://gitlab.com/ntninja/docker-recipies/issues). Alternatively, you may send me a e-mail to [alexander-ws2@ninetailed.ninja](mailto:alexander-ws2@ninetailed.ninja).
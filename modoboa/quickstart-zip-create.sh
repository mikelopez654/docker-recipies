#!/bin/sh
# Create a (mostly) reproducible ZIP for the QuickStart data
set -ue
export TZ=UTC

# Remove previous ZIP file
rm quickstart.zip ||:

# Reset all quickstart directory modification timestamps
find quickstart -exec touch -t 197001010000.00 \{\} \+

# Create new ZIP file without extended attributes and ordered entries
find quickstart \! -name '.git' -print0 | sort -z | xargs -0 zip -X -9 quickstart.zip

#!/bin/sh
set -e

systemctl stop dsrv-modoboa

docker-compose run modoboa update
docker-compose run modoboa manage migrate
echo yes | docker-compose run modoboa manage collectstatic
#docker-compose run modoboa check --deploy

systemctl start dsrv-modoboa